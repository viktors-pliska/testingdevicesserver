const express = require('express');
const mongoose = require('mongoose');
const db = require('../../src/database');
const User = require('../../models/user');

const router = express.Router();

router.get('/', (req, res, next) => {
    User.find()
        .exec()
        .then((docs) => {
            console.log(docs);
            res.status(201).json(docs);
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                error: err,
            });
        });
});

// TODO - Add check if all field was specified for user model
router.post('/', (req, res, next) => {
    const user = new User({
        _id: new mongoose.Types.ObjectId(),
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        title: req.body.title,
    });

    user
        .save()
        .then((result) => {
            console.log(result);
            res.status(201).json({
                createdProduct: result,
            });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                error: err,
            });
        });
});

router.get('/:userId', (req, res, next) => {
    const id = req.params.userId;
    User.findById(id)
        .exec()
        .then((doc) => {
            console.log('From database', doc);
            if (doc) {
                res.status(200).json(doc);
            } else {
                res
                    .status(404)
                    .json({ message: 'No valid entry found for provided ID' });
            }
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({ error: err });
        });
});

module.exports = router;
